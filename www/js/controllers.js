angular.module('app.controllers', [])

  .controller('homeCtrl', function ($scope, $state, $http, tLogHost, $rootScope) {
    $scope.goToUpload = function () {
      $state.go('tabsController.upload');
    };

    $scope.videos = null;

    var init = function() {
      $http({
        method: 'GET',
        url: tLogHost + "/videos/mine"
      }).then(function(resp) {
        console.log(resp);
        $scope.videos = resp.data;
      }).catch(function(err) {
        console.log(err);
      });
    };

    $rootScope.$on("videoAdded", function() {
      init();
    });

    init();
  })

  .controller('loginCtrl', function ($scope, $rootScope, SecurityService, $state) {
    $scope.user = {
      redirect: false
    };

    $scope.error = null;

    $scope.login = function () {
      var error;
      try {
        $rootScope.$on('loggedin', function () {
          console.log(SecurityService.user);
          $state.go('tabsController.profile')
        });
        $rootScope.$on('loginfailed', function () {
          console.log('Error when login!');
          $scope.error = 'Login failed!';
        });
        SecurityService.login($scope.user);
      } catch (_error) {
        console.log(_error);
        $scope.error = 'Login failed!';
      }
    };
  })

  .controller('profileCtrl', function ($scope, SecurityService, $state, $rootScope) {
    $scope.user = SecurityService.user;

    $rootScope.$on('logout', function () {
      console.log('Loggedout');
      $state.go('login');
    });

    $scope.logout = function () {
      SecurityService.logout();
    };
  })

  .controller('settingsCtrl', function ($scope, $state, $translate) {
    $scope.success = null;
    $scope.language = window.localStorage[PREFERENCE_LANGUAGE_KEY];
    console.log('Loaded language from user preferences: ' + $scope.language);

    $scope.change = function (value) {
      $scope.language = value;
    };

    $scope.save = function () {
      window.localStorage[PREFERENCE_LANGUAGE_KEY] = $scope.language;
      $translate.use($scope.language);
      console.log('Language used in app: ' + $scope.language);
      //$state.go('tabsController.profile');
      $translate('LANGUAGE_SUCESS').then(function (msg) {
        $scope.success = msg;
      });
    };
  })

  .controller('uploadCtrl', function ($scope, $cordovaFileTransfer, $cordovaDialogs, $cordovaCamera, $timeout,
                                      $cordovaCapture, tLogHost, $http, $ionicLoading, $state, $rootScope) {

    $scope.videoURL = null;
    $scope.videoData = {};

    var albumOptions = {
      destinationType: Camera.DestinationType.FILE_URI,
      sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
      mediaType: Camera.MediaType.VIDEO
    };

    $scope.selectVideo = function () {
      $ionicLoading.show({
        template: "Selecting video..."
      });

      $cordovaCamera.getPicture(albumOptions)
        .then(function (videoURL) {
          $ionicLoading.hide();
          console.log(videoURL);
          $scope.videoURL = videoURL;
          displayVideo();
        })
        .catch(function (err) {
          $ionicLoading.hide();
          console.log(err);
        });
    };

    $scope.captureVideo = function () {
      $ionicLoading.show({
        template: "Making video..."
      });

      var options = {limit: 1};

      $cordovaCapture.captureVideo(options)
        .then(function (videoData) {
          $ionicLoading.hide();
          $scope.videoURL = videoData[0].fullPath;
          displayVideo();
        })
        .catch(function (err) {
          $ionicLoading.hide();
          console.log(err);
        });
    };

    var displayVideo = function () {
      var v = "<video controls='controls' style='width: 100%'>";
      v += "<source src='" + $scope.videoURL + "' type='video/mp4'>";
      v += "</video>";
      document.querySelector("#videoArea").innerHTML = v;
    };

    $scope.upload = function () {
      $ionicLoading.show({
        template: "Uploading video..."
      });
      $http({
        method: "POST",
        url: tLogHost + "/videos",
        data: {
          information: ($scope.videoData.information ? $scope.videoData.information : "")
        },
        headers: {
          Authorization: "Bearer " + localStorage.getItem('JWT')
        }
      })
        .then(function (video) {
          console.log("Success " + video.data._id);
          addVideo(video.data._id);
        })
        .catch(function (err) {
          $ionicLoading.hide();
          console.log("Error");
        });
    };

    var addVideo = function (videoId) {
      $cordovaFileTransfer.upload(tLogHost + "/videos/videofile/" + videoId, $scope.videoURL,
        {
          headers: {
            Authorization: "Bearer " + (localStorage.getItem('JWT'))
          }
        })
        .then(function () {
          $ionicLoading.hide();
          $timeout(function() {
            $rootScope.$emit('videoAdded');
          });
          $state.go('tabsController.home');
          console.log("Successful upload");
        })
        .catch(function (err) {
          console.log("Error" + err);
          $ionicLoading.hide();
        });
    };

  });
