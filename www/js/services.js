'use strict';

var services = angular.module('app.services', ['ngResource', 'ngCookies']);

services.value('tLogHost', 'http://localhost:3000/api');
//services.value('tLogHost', 'http://192.168.0.4:3000/api');

services.factory('SecurityService', ["$rootScope", "$http", "$location", "$stateParams", "$q", "$timeout", "tLogHost", "$state", "$cookies", "$cookieStore", function ($rootScope, $http, $location, $stateParams, $q, $timeout, tLogHost, $state, $cookies, $cookieStore) {
  var MeanUser, MeanUserKlass, b64_to_utf8, escape, self, tokenWatch;
  self = void 0;
  escape = function (html) {
    return String(html).replace(/&/g, '&amp;').replace(/"/g, '"').replace(/'/g, '&#39;').replace(/</g, '<').replace(/>/g, '>');
  };
  b64_to_utf8 = function (str) {
    return decodeURIComponent(escape(window.atob(str)));
  };
  MeanUserKlass = function () {
    this.name = 'users';
    this.user = {};
    this.registerForm = false;
    this.loggedin = false;
    this.isAdmin = false;
    this.loginError = 0;
    this.usernameError = null;
    this.registerError = null;
    this.resetpassworderror = null;
    this.validationError = null;
    self = this;
    $http.get(tLogHost + "/users/me").success(self.onIdentity);
  };
  MeanUserKlass.prototype.onIdentity = function (response) {
    var destination, encodedProfile, payload;
    self.loginError = 0;
    self.loggedin = true;
    self.registerError = 0;
    if (!response) {
      self.user = {};
      self.loggedin = false;
      self.isAdmin = false;
    } else if (angular.isDefined(response.token)) {
      localStorage.setItem('JWT', response.token);
      encodedProfile = decodeURI(b64_to_utf8(response.token.split('.')[1]));
      payload = JSON.parse(encodedProfile);
      self.user = payload;
      destination = $cookies.get('redirect');
      if (self.user.roles.indexOf('admin') !== -1) {
        self.isAdmin = true;
      }
      $rootScope.$emit('loggedin');
      if (destination) {
        $location.path(destination.replace(/^"|"$/g, ''));
        $cookieStore.remove('redirect');
      } else {
        $location.url('/');
      }
    } else {
      self.user = response;
      self.loggedin = true;
      if (self.user.roles.indexOf('admin') !== -1) {
        self.isAdmin = true;
      }
      $rootScope.$emit('loggedin');
    }
  };
  MeanUserKlass.prototype.onIdFail = function (response) {
    console.log(JSON.stringify(response));
    $location.path(response.redirect);
    self.loginError = 'Authentication failed.';
    self.registerError = response;
    self.validationError = response.msg;
    self.resetpassworderror = response.msg;
    $rootScope.$emit('loginfailed');
    $rootScope.$emit('registerfailed');
  };
  MeanUser = new MeanUserKlass;
  MeanUserKlass.prototype.login = function (user) {
    var destination;
    destination = $location.path().indexOf('/login') === -1 ? $location.absUrl() : false;
    $http.post(tLogHost + "/login", {
      email: user.email,
      password: user.password,
      redirect: destination
    }).success(self.onIdentity).error(self.onIdFail);
  };
  MeanUserKlass.prototype.register = function (user) {
    $http.post(tLogHost + "/register", {
      email: user.email,
      password: user.password,
      confirmPassword: user.confirmPassword,
      username: user.username,
      name: user.username
    }).success(self.onIdentity).error(self.onIdFail);
  };
  MeanUserKlass.prototype.resetpassword = function (user) {
    $http.post((tLogHost + "/reset/") + $stateParams.tokenId, {
      password: user.password,
      confirmPassword: user.confirmPassword
    }).success(self.onIdentity).error(self.onIdFail);
  };
  MeanUserKlass.prototype.forgotpassword = function (user) {
    $http.post(tLogHost + "/forgot-password", {
      text: user.email
    }).success(function (response) {
      $rootScope.$emit('forgotmailsent', response);
    }).error(self.onIdFail);
  };
  MeanUserKlass.prototype.logout = function () {
    self.user = {};
    self.loggedin = false;
    self.isAdmin = false;
    $http.get(tLogHost + "/logout").success(function (data) {
      localStorage.removeItem('JWT');
      $rootScope.$emit('logout');
    });
  };
  MeanUserKlass.prototype.isLoggedIn = function () {
    return $http.get(tLogHost + "/loggedin").then(function (resp) {
      if (resp.data !== '0') {
        $rootScope.$emit('loginstatus', true);
      } else {
        $rootScope.$emit('loginstatus', false);
      }
    }, function (err) {
      console.log("================ ERROR ========");
      console.log(JSON.stringify(err));
      return console.log(" ================= /ERROR ===========");
    });
  };
  MeanUserKlass.prototype.checkLoggedin = function () {
    var deferred;
    deferred = $q.defer();
    $http.get(tLogHost + "/loggedin").then(function (resp) {
      if (resp.data !== '0') {
        $rootScope.$emit('loginstatus', true);
        $timeout(deferred.resolve);
      } else {
        $cookieStore.put('redirect', $location.path());
        $rootScope.$emit('loginstatus', true);
        $timeout(deferred.reject);
        $location.url("/login");
      }
    }, function (err) {
      console.log("================ ERROR ========");
      console.log(JSON.stringify(err));
      return console.log(" ================= /ERROR ===========");
    });
    return deferred.promise;
  };
  MeanUserKlass.prototype.checkLoggedOut = function () {
    var deferred;
    deferred = $q.defer();
    $http.get(tLogHost + "/loggedin").success(function (user) {
      if (user !== '0') {
        $timeout(deferred.reject);
        $location.url('/');
      } else {
        $timeout(deferred.resolve);
      }
    });
    return deferred.promise;
  };
  MeanUserKlass.prototype.checkAdmin = function () {
    var deferred;
    deferred = $q.defer();
    $http.get(tLogHost + "/loggedin").success(function (user) {
      if (user !== '0' && user.roles.indexOf('admin') !== -1) {
        $timeout(deferred.resolve);
      } else {
        $timeout(deferred.reject);
        $location.url('/#');
      }
    });
    return deferred.promise;
  };
  tokenWatch = $rootScope.$watch((function () {
    return $cookies.get('token');
  }), function (newVal, oldVal) {
    if (newVal && newVal !== void 0 && newVal !== null && newVal !== '') {
      self.onIdentity({
        token: $cookies.get('token')
      });
      $cookieStore.remove('token');
      tokenWatch();
    }
  });
  return MeanUser;
}]);
