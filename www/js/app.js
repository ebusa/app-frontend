// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
var app = angular.module('app', ['ionic', 'app.controllers', 'app.routes', 'app.services', 'app.directives', 'ngCordova', 'pascalprecht.translate']);

app.run(function ($ionicPlatform) {
  $ionicPlatform.ready(function () {

    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
});

const PREFERENCE_LANGUAGE_KEY = 'PREFERENCE_LANGUAGE_KEY';
const LANGUAGES = ['en', 'de'];

app.config(function ($translateProvider, $ionicConfigProvider) {
  $ionicConfigProvider.tabs.position('bottom');
  $translateProvider.useStaticFilesLoader({
    prefix: 'trans/',
    suffix: '.json'
  });
  $translateProvider.useSanitizeValueStrategy('escape');

  var language = loadLangugagePreference();
  if (language !== undefined && language !== null) {
    console.log('Loaded language from user preferences: ' + language);
    language = LANGUAGES.indexOf(language) >= 0 ? language : 'en';
  } else {
    language = navigator.language !== '' ? navigator.language : 'en';
    console.log('Loaded language from device: ' + language);
    language = LANGUAGES.indexOf(language) >= 0 ? language : 'en';
  }
  console.log('Language used in app: ' + language);
  $translateProvider.use(language);
  saveLanguagePreference(language);
});

function loadLangugagePreference() {
  return window.localStorage[PREFERENCE_LANGUAGE_KEY];
}

function saveLanguagePreference(language) {
  window.localStorage[PREFERENCE_LANGUAGE_KEY] = language;
}

