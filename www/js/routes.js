angular.module('app.routes', ['angular-jwt'])

  .config(function ($stateProvider, $urlRouterProvider, $httpProvider, jwtInterceptorProvider) {
    $httpProvider.defaults.withCredentials = true;
    jwtInterceptorProvider.tokenGetter = function () {
      return localStorage.getItem("JWT");
    };
    $httpProvider.interceptors.push('jwtInterceptor');

    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $stateProvider
      .state('tabsController', {
        url: '/tabs',
        templateUrl: 'templates/tabsController.html',
        abstract: true
      })
      .state('tabsController.home', {
        url: '/home',
        views: {
          'tab1': {
            templateUrl: 'templates/home.html',
            controller: 'homeCtrl'
          }
        },
        resolve: {
          loggedIn: ["SecurityService", function (SecurityService) {
            return SecurityService.checkLoggedin();
          }]
        }
      })
      .state('login', {
        url: '/login',
        templateUrl: 'templates/login.html',
        controller: 'loginCtrl'
      })
      .state('tabsController.profile', {
        url: '/profile',
        views: {
          'tab2': {
            templateUrl: 'templates/profile.html',
            controller: 'profileCtrl'
          }
        }
      }).state('tabsController.settings', {
      url: '/settings',
      views: {
        'tab2': {
          templateUrl: 'templates/settings.html',
          controller: 'settingsCtrl'
        }
      }
    }).state('tabsController.upload', {
      url: '/upload',
      views: {
        'tab1': {
          templateUrl: 'templates/upload.html',
          controller: 'uploadCtrl'
        }
      }
    });

    $urlRouterProvider.otherwise('/tabs/home')
  });
